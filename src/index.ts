/**
 * Ethr
 */
export interface Ethr<L, R> {
    isLeft(): boolean;
    isRight(): boolean;
    mapL<T>(f: (value: L) => T): Ethr<T, R>;
    mapR<T>(f: (value: R) => T): Ethr<L, T>;
    fold<NL, NR>(left: (lvalue: L) => NL, right: (rvalue: R) => NR): NL | NR;
    fromLeft<T>(defaultValue: T): T | L;
    fromRight<T>(defaultValue: T): T | R;
    chainL<T>(f: (lvalue: L) => Ethr<T, R>): Ethr<T, R>;
    chainR<T>(f: (rvalue: R) => Ethr<L, T>): Ethr<L, T>;
    bimap<NL, NR>(left: (lvalue: L) => NL, right: (rvalue: R) => NR): Ethr<NL, NR>;
}

export function Left<T>(value: T): Ethr<T, never> {
    return ({
        isLeft: function() { return true; },
        isRight: function() { return false; },
        mapL: function(f) { return Left(f(value)); },
        mapR: function(_) { return Left(value); },
        fold: function(left, _) { return left(value); },
        fromLeft: function(_) { return value; },
        fromRight: function(defaultValue) { return defaultValue; },
        chainL: function(f) { return f(value); },
        chainR: function(_) { return Left(value); },
        bimap: function(f, _) { return Left(f(value)); },
    });
} 

export function Right<T>(value: T): Ethr<never, T> {
    return ({
        isLeft: function() { return false; },
        isRight: function() { return true; },
        mapL: function(_) { return Right(value); },
        mapR: function(f) { return Right(f(value)); },
        fold: function(_, right) { return right(value); },
        fromLeft: function(defaultValue) { return defaultValue; },
        fromRight: function(_) { return value; },
        chainL: function(_) { return Right(value); },
        chainR: function(f) { return f(value); },
        bimap: function(_, f) { return Right(f(value)); },
    });
} 

export function Ethr<L, R>(condition: boolean, lvalue: L, rvalue: R): Ethr<L, R> {
    return condition ? Right(rvalue) : Left(lvalue);
}
